package com.kouasseu.model;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/07/2023 -- 15:36<br></br>
 * By : @author alexk<br></br>
 * Project : data-access-java17<br></br>
 * Package : com.kouasseu.model<br></br>
 */
public class Book {
    private long id;
    private String title;

    private int rating;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public static BookBuilder builder() {
        return new BookBuilder();
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", rating=" + rating +
                '}';
    }

    public static final class BookBuilder {
        private long id;
        private String title;
        private int rating;


        public BookBuilder withId(long id) {
            this.id = id;
            return this;
        }

        public BookBuilder withTitle(String title) {
            this.title = title;
            return this;
        }
        public BookBuilder withRating(int rating) {
            this.rating = rating;
            return this;
        }

        public Book build() {
            Book book = new Book();
            book.setId(id);
            book.setTitle(title);
            book.setRating(rating);
            return book;
        }
    }
}
