package com.kouasseu.repository;

import java.util.List;
import java.util.Optional;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/07/2023 -- 15:34<br></br>
 * By : @author alexk<br></br>
 * Project : data-access-java17<br></br>
 * Package : com.kouasseu.repository<br></br>
 */
public interface Dao<T> {
    Optional<T> findById(long id);
    List<T> findAll();
    T save(T book);

    T update(T book);

    int[] update(List<T> t);

    int delete(T t);
}
