package com.kouasseu.repository;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/07/2023 -- 15:30<br></br>
 * By : @author alexk<br></br>
 * Project : data-access-java17<br></br>
 * Package : com.kouasseu.repository<br></br>
 */
public record RepositoryProperties(String url, String username, String password) {
}
