package com.kouasseu.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/07/2023 -- 16:52<br></br>
 * By : @author alexk<br></br>
 * Project : data-access-java17<br></br>
 * Package : com.kouasseu.repository<br></br>
 */
public abstract class JdbcQueryTemplate<T> extends AbstractDao {

    public JdbcQueryTemplate() {

    }


    public List<T> queryForList(String sql) {
        List<T> items = new ArrayList<>();

        try(    Connection con = getConnection();
                Statement stmt = con.createStatement();
                ResultSet set = stmt.executeQuery(sql)
        ) {
               while (set.next()) {
                   items.add(mapItem(set));
               }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return items;
    }

    public abstract T mapItem(ResultSet set) throws SQLException;

}

