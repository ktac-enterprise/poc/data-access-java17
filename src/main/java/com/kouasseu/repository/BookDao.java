package com.kouasseu.repository;

import com.kouasseu.model.Book;

import java.sql.*;
import java.util.List;
import java.util.Optional;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/07/2023 -- 15:37<br></br>
 * By : @author alexk<br></br>
 * Project : data-access-java17<br></br>
 * Package : com.kouasseu.repository<br></br>
 */
public class BookDao extends AbstractDao implements Dao<Book> {
    @Override
    public Optional<Book> findById(long id) {
        Optional<Book> book = Optional.empty();
        String sql = "SELECT ID, TITLE FROM BOOK WHERE ID = ?";

        try (
                Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(sql);
        ) {

            statement.setLong(1, id);

            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    Book resBook = Book.builder()
                            .withId(set.getLong("id"))
                            .withTitle(set.getString("title"))
                            .build();

                    book = Optional.of(resBook);
                }

            }

        } catch (SQLException e) {e.printStackTrace();}

        return book;
    }

    @Override
    public List<Book> findAll() {
        JdbcQueryTemplate<Book> template = new JdbcQueryTemplate<>() {
            @Override
            public Book mapItem(ResultSet set) throws SQLException {
                return  Book.builder()
                        .withId(set.getLong("ID"))
                        .withTitle(set.getString("TITLE"))
                        .withRating(set.getInt("RATING"))
                        .build();
            }
        };

        return template.queryForList("SELECT * FROM BOOK");
    }

   /* @Override
    public List<Book> findAll() {
        List<Book> books = new ArrayList<>();

        String sql = "SELECT * FROM BOOK";

        try (
                Connection con = getConnection();
                Statement statement = con.createStatement();
                ResultSet set = statement.executeQuery(sql)
        ) {
            while (set.next()) {
                Book book = Book.builder()
                        .withId(set.getLong("id"))
                        .withTitle(set.getString("title"))
                        .build();
                books.add(book);
            }

        } catch (SQLException e) {e.printStackTrace();}
        return books;
    }*/

    @Override
    public Book save(Book book) {
        String sql = "INSERT INTO BOOK (TITLE) VALUES (?)";

        try (
                Connection con = getConnection();
                PreparedStatement prepStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ){
            prepStatement.setString(1, book.getTitle());
            prepStatement.executeUpdate();

            try (ResultSet genKeys = prepStatement.getGeneratedKeys()){
                if(genKeys.next()) {
                    book.setId(genKeys.getLong(1));
                }
            }
        } catch (SQLException e) {e.printStackTrace();}

        return book;
    }

    @Override
    public Book update(Book book) {
        String sql = "UPDATE BOOK SET TITLE = ? WHERE ID = ?";

        try (
                Connection con = getConnection();
                PreparedStatement prepStatement = con.prepareStatement(sql)
        ){
            prepStatement.setString(1, book.getTitle());
            prepStatement.setLong(2, book.getId());
            prepStatement.executeUpdate();
        } catch (SQLException e) {e.printStackTrace();}

        return book;
    }

    // Can also be applies with a batch delete
    @Override
    public int[] update(List<Book> books) {
        int [] records = {};

        String sql = "UPDATE BOOK SET TITLE = ?, RATING = ? WHERE ID = ?";

        try (
                Connection con = getConnection();
                PreparedStatement prepStatement = con.prepareStatement(sql)
        ){
            for (Book book : books) {
                prepStatement.setString(1, book.getTitle());
                prepStatement.setLong(2, book.getRating());
                prepStatement.setLong(3, book.getId());

                prepStatement.addBatch();
            }

            records = prepStatement.executeBatch();
        } catch (SQLException e) {e.printStackTrace();}
        return records;
    }

    @Override
    public int delete(Book book) {
        int rowsAffected = 0;

        String sql = "DELETE FROM BOOK WHERE ID = ?";

        try (
                Connection con = getConnection();
                PreparedStatement prepStatement = con.prepareStatement(sql)
        ){
                prepStatement.setLong(1, book.getId());

            rowsAffected = prepStatement.executeUpdate();
        } catch (SQLException e) {e.printStackTrace();}

        return rowsAffected;
    }
}
