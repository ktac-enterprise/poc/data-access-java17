package com.kouasseu.repository;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/07/2023 -- 15:25<br></br>
 * By : @author alexk<br></br>
 * Project : data-access-java17<br></br>
 * Package : com.kouasseu.repository<br></br>
 */
public class AbstractDao {

    protected Connection getConnection() throws SQLException {
        RepositoryProperties properties = loadDatabaseProperties();
        return DriverManager.getConnection(properties.url(), properties.username(), properties.password());
    }

    private  RepositoryProperties loadDatabaseProperties() {
        try (InputStream propertiesStream = AbstractDao.class.getResourceAsStream("/repository.properties")) {
            Properties properties = new Properties();
            properties.load(propertiesStream);
            return new RepositoryProperties(properties.getProperty("app.datasource.url"),
                    properties.getProperty("app.datasource.username"),
                    properties.getProperty("app.datasource.password"));
        } catch (IOException e) {
            throw new IllegalArgumentException("Cloud not load properties file");
        }
    }
}
