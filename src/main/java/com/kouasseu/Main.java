package com.kouasseu;

import com.kouasseu.model.Book;
import com.kouasseu.repository.BookDao;
import com.kouasseu.repository.Dao;

import java.util.List;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  23/07/2023 -- 15:16<br></br>
 * By : @author alexk<br></br>
 * Project : Default (Template) Project<br></br>
 * Package : com.kouasseu<br></br>
 */
public class Main {
    public static void main(String[] args) {
        Dao<Book> bookDao = new BookDao();

        List<Book> books = bookDao.findAll();

        for (Book book: books) {
            System.out.println("Book : " + book);
        }

       /* Optional<Book> optionalBook = bookDao.findById(1);

        optionalBook.ifPresent(book -> {
            System.out.println("book = " + book);

            book.setTitle("La clé de la maitrise: Seconde Edition");
            bookDao.update(book);
        });*/

       /* books = bookDao.findAll();
        List<Book> updatedEntries = books.stream()
                .peek(book -> book.setRating(5))
                .toList();

        int [] records = bookDao.update(updatedEntries);

        System.out.println("records = " + Arrays.toString(records));*/
        /*Book newBook = Book.builder()
                .withTitle("Book to delete")
                .build();

        newBook = bookDao.save(newBook);
        System.out.println("newBook = " + newBook);

        int numDel = bookDao.delete(newBook);
        System.out.println("Number of records deleted = " + numDel);*/

    }
}